﻿using UnityEngine;
using System.Collections;

namespace Primordia {

	[RequireComponent (typeof(Rigidbody))]

	// the aesthetic projectile is projectile that is supposed to hit (it's spawning ability is not supposed to miss)
	public class AestheticProjectile : MonoBehaviour {
		
		private Transform m_start;
		public Transform start{
			get{
				return m_start;
			}
			set{
				m_start = value;
			}
		}

		private Transform m_target;
		public Transform target{
			get{
				return m_target;
			}
			set{
				m_target = value;
			}
		}

		public float flightSpeed;

		public float damage;
		public Pawn.DamageType damageType;
		
		IEnumerator WaitAndDamage(float waitTime) {
			yield return new WaitForSeconds(waitTime);
			if (target != null && target.gameObject != null && target.gameObject.GetComponent<Pawn> () != null) {
				target.gameObject.GetComponent<Pawn> ().Damage (damage, damageType);
			}
			// we should have impacted by now, so we destroy ourselves
			Destroy (this.gameObject);
		}
			
		public void Deploy () {
			// heh, this method is pretty fucked. Fix it if you care.

			float distance = Vector3.Distance (start.position, target.position); 

			// we know exactly how long the projectile will be in flight if it is ballistic (arrow, etc.)
			float flightTime = distance / flightSpeed;

			// since we don't actually care if the projectile hits (it just has to look good),
			// we can send a delayed damage command now
			StartCoroutine(WaitAndDamage(flightTime));

			// since we know the flight time, we know the horizontal velocity (no drag!)
			float x_vel = distance / flightTime;

			// now we need to calculate the intial vertical velocity that will result in the desired flight time
			// flightTime = distance / cos(theta)
			//float cos_Theta = distance / flightTime;
			//print (cos_Theta);
			//float theta = Mathf.Acos (cos_Theta);

			//x_vel = vel * cos_Theta
			//float velocity = x_vel / cos_Theta;

			float y_vel = -Physics.gravity.y * flightTime/2;

			// find the direction of travel
			Vector3 direction = target.position - start.position;
			Vector2 horizontalDirection = new Vector2 (direction.x, direction.z);
			horizontalDirection.Normalize ();

			// set up the rigidbody
			transform.position = start.position + 0.5f*Vector3.up;
			rigidbody.velocity = new Vector3 (horizontalDirection.x * x_vel, y_vel, horizontalDirection.y * x_vel);
		}
		
		// Update is called once per frame
		void FixedUpdate () {
			// TODO check and fix course
			transform.LookAt (transform.position + rigidbody.velocity * Time.fixedDeltaTime);
		}
	}

}
