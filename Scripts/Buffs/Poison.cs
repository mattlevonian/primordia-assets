﻿using UnityEngine;
using System.Collections;

namespace Primordia {

public class Poison : Buff {

	public float damagePerSecond;
	protected float lastDamageTime;

	protected override void OnSpawn(){
		lastDamageTime = Time.time;
	}

	protected override void OnUpdate(){
		if (Time.time - lastDamageTime > 1f) {
			this.GetComponent<Pawn>().Damage(damagePerSecond);
			lastDamageTime = Time.time;
		}
	}

	protected override void OnExpiration(){}

}

}