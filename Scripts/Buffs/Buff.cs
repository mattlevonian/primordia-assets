﻿using UnityEngine;
using System.Collections;

namespace Primordia {

[RequireComponent (typeof (Pawn))]
public abstract class Buff : MonoBehaviour {

	public bool canExpire;
	public float duration;

	protected float startTime;
	
	void Start () {
		// TODO better timekeeping system?
		startTime = Time.time;
		OnSpawn();
	}

	void Update () {
		if (canExpire && startTime + duration < Time.time) {
			OnExpiration();
			Destroy(this);
		}else{
			OnUpdate();
		}
	}

	protected abstract void OnSpawn();
	protected abstract void OnUpdate();
	protected abstract void OnExpiration();
}

}