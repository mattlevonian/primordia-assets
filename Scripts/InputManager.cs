﻿using UnityEngine;
using System.Collections;

namespace Primordia {

// Input Manager records user input and tells the PC (player character) objects about it, letting them adjust their internal states accordingly

public class InputManager : MonoBehaviour {

	// material used when rendering selection box (marque)
	public Material marqueMaterial;

	// width of selection marque lines (in pixels)
	public int marqueLineWidth;

	// a world point that defines the start of a selection box
	private Vector3 selection_start;
	private Vector3 selection_start_screenpoint;
	private bool first_raycast_valid;
	
	// Update is called once per frame
	void Update () {

		// ---- SELECTION INPUT VIA LEFT CLICK ----

		// start left click (prepare for drag selection)
		if (Input.GetMouseButtonDown (0)) {

			//Debug.Log("Left Mouse Button Down");

			selection_start_screenpoint = Input.mousePosition;

			RaycastHit hit = new RaycastHit();
			first_raycast_valid = RayCastMousePosition(out hit);

			selection_start = hit.point;// + Vector3.up * 5;
		}

		// end left click (select stuff inside box)
		if (Input.GetMouseButtonUp (0)) {

			//Debug.Log("Left Mouse Button Up");

			RaycastHit hit = new RaycastHit();
			bool didRaycastHit = RayCastMousePosition(out hit);

			Vector3 selection_end = hit.point;

			// ---- Deselect Everything ----

			// TODO modifier key to allow multiple selection?
			foreach (GameObject ch in GameObject.FindGameObjectsWithTag(Constants.PLAYER_CHAR_TAG)) {
				if(ch.GetComponent<PlayerCharacter>() != null){
					ch.GetComponent<PlayerCharacter>().selected = false;
				}
			}

			// check if the user is actually over a PC
			hit = new RaycastHit();
			if(RayCastMousePositionToPawns(out hit)){
				if(hit.collider.gameObject.GetComponent<PlayerCharacter>() != null){
					hit.collider.gameObject.GetComponent<PlayerCharacter>().selected = true;
				}
			}

			if(didRaycastHit && first_raycast_valid){

				// ---- Select All Within Bounds ----

				// TODO better way to do this?
				Bounds bounds = new Bounds(0.5f*(selection_end+selection_start), new Vector3(0,0,0));
				bounds.Encapsulate(selection_end);
				bounds.Encapsulate(selection_start);

				bounds.size = new Vector3(bounds.size.x, 5, bounds.size.z);

				foreach (GameObject ch in GameObject.FindGameObjectsWithTag(Constants.PLAYER_CHAR_TAG)) {
					if(bounds.Contains(ch.transform.position) && ch.GetComponent<PlayerCharacter>() != null){
						ch.GetComponent<PlayerCharacter>().selected = true;
					}
				}
			}
		}

		// ---- KEYBOARD SELECTION ----

		if(Input.GetButtonDown(Constants.SELECT_KEY_1)){
			foreach (GameObject ch in GameObject.FindGameObjectsWithTag(Constants.PLAYER_CHAR_TAG)) {
				ch.GetComponent<PlayerCharacter>().selected = false;
			}
			foreach (GameObject ch in GameObject.FindGameObjectsWithTag(Constants.PLAYER_CHAR_TAG)) {
				if(ch.GetComponent<PlayerCharacter>() != null && ch.GetComponent<PlayerCharacter>().characterOrder == 1){
					ch.GetComponent<PlayerCharacter>().selected = true;
				}
			}
		}
		if(Input.GetButtonDown(Constants.SELECT_KEY_2)){
			foreach (GameObject ch in GameObject.FindGameObjectsWithTag(Constants.PLAYER_CHAR_TAG)) {
				ch.GetComponent<PlayerCharacter>().selected = false;
			}
			foreach (GameObject ch in GameObject.FindGameObjectsWithTag(Constants.PLAYER_CHAR_TAG)) {
				if(ch.GetComponent<PlayerCharacter>() != null && ch.GetComponent<PlayerCharacter>().characterOrder == 2){
					ch.GetComponent<PlayerCharacter>().selected = true;
				}
			}
		}
		if(Input.GetButtonDown(Constants.SELECT_KEY_3)){
			foreach (GameObject ch in GameObject.FindGameObjectsWithTag(Constants.PLAYER_CHAR_TAG)) {
				ch.GetComponent<PlayerCharacter>().selected = false;
			}
			foreach (GameObject ch in GameObject.FindGameObjectsWithTag(Constants.PLAYER_CHAR_TAG)) {
				if(ch.GetComponent<PlayerCharacter>() != null && ch.GetComponent<PlayerCharacter>().characterOrder == 3){
					ch.GetComponent<PlayerCharacter>().selected = true;
				}
			}
		}
		if(Input.GetButtonDown(Constants.SELECT_KEY_4)){
			foreach (GameObject ch in GameObject.FindGameObjectsWithTag(Constants.PLAYER_CHAR_TAG)) {
				ch.GetComponent<PlayerCharacter>().selected = false;
			}
			foreach (GameObject ch in GameObject.FindGameObjectsWithTag(Constants.PLAYER_CHAR_TAG)) {
				if(ch.GetComponent<PlayerCharacter>() != null && ch.GetComponent<PlayerCharacter>().characterOrder == 4){
					ch.GetComponent<PlayerCharacter>().selected = true;
				}
			}
		}

		// ---- MOVE AND CAST ----

		// end right click (move or cast at target)
		if (Input.GetMouseButtonUp (1)) {
			RaycastHit hit = new RaycastHit();
			if(RayCastMousePositionToPawns(out hit) && hit.collider.gameObject.GetComponent<Pawn>()!=null){
				// attempt to tell the selected characters to cast
				foreach (GameObject ch in GameObject.FindGameObjectsWithTag(Constants.PLAYER_CHAR_TAG)) {
					if(ch.GetComponent<PlayerCharacter>() != null && ch.GetComponent<PlayerCharacter>().selected){
						ch.GetComponent<PlayerCharacter>().SetCastAtEntity(hit.collider.gameObject);
						ch.GetComponent<PlayerCharacter>().FireDefaultCastable();
						//Debug.Log (ch.ToString()+" firing default castable at "+hit.collider.gameObject.ToString());
					}
				}
			}
			else 
			if(RayCastMousePosition(out hit)){
				foreach (GameObject ch in GameObject.FindGameObjectsWithTag(Constants.PLAYER_CHAR_TAG)) {
					if(ch.GetComponent<PlayerCharacter>() != null && ch.GetComponent<PlayerCharacter>().selected){
						// TODO formations, navigate to non-interfering points around that area instead of running into each other
						ch.GetComponent<PlayerCharacter>().MoveToPoint(hit.point);
					}
				}
			}
		}

		// ---- OTHER INPUTS ----

		// TODO other input stuff
	}

	private bool RayCastMousePosition (out RaycastHit hitinfo){
		// find the world point that corresponds to the click
		Ray clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		
		// TODO proper, robust raycast
		int layerMask = 1 << 8;
		return Physics.Raycast(clickRay, out hitinfo, 100f, layerMask);
	}
	private bool RayCastMousePositionToPawns (out RaycastHit hitinfo){
		// find the world point that corresponds to the click
		Ray clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		
		// TODO proper, robust raycast
		int layerMask = 1 << 9;
		return Physics.Raycast(clickRay, out hitinfo, 100f, layerMask);
	}

	void OnGUI ()
	{
		// draw selection box while mouse button is down
		if (first_raycast_valid && Input.GetMouseButton(0)) {
			// draw 2D box from selection_start_screenpoint to Input.mousePosition

			//Debug.Log("Drawing selection marque");

			DrawSelectionBox (new Rect(selection_start_screenpoint.x,
			                           Screen.height - selection_start_screenpoint.y,
			                           Input.mousePosition.x - selection_start_screenpoint.x,
			                           -(Input.mousePosition.y - selection_start_screenpoint.y)),
			                  Color.green);
		}

	}

	// thanks to http://answers.unity3d.com/questions/37752/how-to-render-a-colored-2d-rectangle.html for the GL help
	// draw a 2D while drag selecting
	void DrawSelectionBox (Rect position, Color color)
	{

		// We shouldn't draw until we are told to do so.
		//if (Event.current.type != EventType.Repaint)
		//	return;

		// Please assign a material that is using position and color.
		if (marqueMaterial == null) {
			Debug.LogError ("You have forgot to set a material for InputManager.");
			return;
		}
		marqueMaterial.SetPass (0);
		// Optimization hint:
		// Consider Graphics.DrawMeshNow
		GL.Color (color);
		GL.Begin (GL.QUADS);

		GL.Vertex3 (position.x, position.y, 0);
		GL.Vertex3 (position.x + position.width, position.y, 0);
		GL.Vertex3 (position.x + position.width, position.y - marqueLineWidth, 0);
		GL.Vertex3 (position.x, position.y - marqueLineWidth, 0);

		GL.Vertex3 (position.x + position.width, position.y, 0);
		GL.Vertex3 (position.x + position.width, position.y+ position.height, 0);
		GL.Vertex3 (position.x + position.width - marqueLineWidth, position.y+ position.height, 0);
		GL.Vertex3 (position.x + position.width - marqueLineWidth, position.y, 0);

		GL.Vertex3 (position.x + position.width, position.y+ position.height, 0);
		GL.Vertex3 (position.x, position.y + position.height, 0);
		GL.Vertex3 (position.x, position.y + position.height - marqueLineWidth, 0);
		GL.Vertex3 (position.x + position.width, position.y+ position.height - marqueLineWidth, 0);

		GL.Vertex3 (position.x, position.y + position.height, 0);
		GL.Vertex3 (position.x, position.y, 0);
		GL.Vertex3 (position.x + marqueLineWidth, position.y, 0);
		GL.Vertex3 (position.x + marqueLineWidth, position.y + position.height, 0);

		GL.End ();
	}
}

}
