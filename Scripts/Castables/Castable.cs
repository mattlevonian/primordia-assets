﻿using UnityEngine;
using System.Collections;

namespace Primordia {

[RequireComponent (typeof (Pawn))]
public abstract class Castable : MonoBehaviour {

	// set in children to unique ID
	protected string m_castID;
	public string castID{
		get{
			return m_castID;
		}
	}

	// can be a default castable (if multiple defaults on a pawn, first one encountered is selected
	public bool isDefault;

	// last time (from Time.time) we cast
	private float lastFire;
	// number of seconds between castings
	public  float cooldown;

	// is this ability range-limited
	public bool hasRange;
	// how far away can the target be?
	public float range;

	// does this castable fire at an entity? If not, it fires at a point on the ground
	public bool targetsEntity;
	// can target self?
	public bool canTargetSelf;

	// an entity to cast at
	protected GameObject m_entityTarget;
	public virtual GameObject entityTarget {
		set{
			m_entityTarget = value;
		}
	}
	// a point in the world to cast at
	protected Vector3 m_worldTarget;
	public Vector3 worldTarget {
		set{
			m_worldTarget = value;
		}
	}

	// can this castable be made to automatically fire repeatedly?
	public bool canAutocast;

	// is this castable currently autocasting?
	protected bool m_isAutocasting;
	public bool isAutocasting {
		get{
			return m_isAutocasting;
		}
		set{
			m_isAutocasting = value;
		}
	}

	// Use this for initialization
	protected virtual void Start () {
		m_isAutocasting = canAutocast; //false;
		m_castID = "AbstractCastable";
	}
	
	// Update is called once per frame
	protected void Update () {
		// TODO more robust time-keeping system?
		if (m_isAutocasting && canAutocast && Time.time - lastFire > cooldown) {
			Cast();
		}

		if ((targetsEntity && m_entityTarget != null) || (!targetsEntity)) {
			if (!InRange ()) {
				GetInRange ();
			} 
		}
	}

	// the external hook that fires off the castable; return false if it didn't fire
	public bool Cast(){
		if (Time.time - lastFire < cooldown) {
			return false;
		}
		if((targetsEntity&&m_entityTarget!=null) || (!targetsEntity)){
			//print (InRange());
			if(InRange()){
				Fire ();
				lastFire = Time.time;
			}else{
				GetInRange();
			}
		}
		return true;
	}

	protected Vector3 TargetPosition(){
		if (targetsEntity) {
			return m_entityTarget.transform.position;
		} else {
			return m_worldTarget;
		}
	}

	protected bool InRange(){
		return !hasRange || range >= Vector3.Distance(transform.position,TargetPosition());
	}

	protected void GetInRange(){
		// determine nearest point that works
		Vector3 direction = transform.position - TargetPosition();
		direction.Normalize();
		
		Vector3 nearestPoint = TargetPosition() + direction * (range - 0.5f);
		gameObject.GetComponent<Pawn>().MoveToPoint_LowPriority(nearestPoint);
	}

	// the method you override in children to do stuff
	protected abstract void Fire ();

}

}
