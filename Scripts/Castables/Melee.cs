﻿using UnityEngine;
using System.Collections;

namespace Primordia {

public class Melee : Castable {

	public float damagePerHit;

	public override GameObject entityTarget {
		set{
			if((canTargetSelf || !value.Equals(gameObject)) && value.GetComponent<Pawn>() != null && !value.GetComponent<Pawn>().alliance.Equals(gameObject.GetComponent<Pawn>().alliance)){
				//print (gameObject.GetComponent<Pawn>().alliance.ToString() + " vs " + value.GetComponent<Pawn>().alliance.ToString());
				m_entityTarget = value;
				isAutocasting = true;
			}
		}
	}

	// Use this for initialization
	protected override void Start () {
		base.Start ();
		m_castID = "BaseMelee";
		//canAutocast = true;
		//targetsEntity = true;
	}

	// the method you override in children to do stuff
	protected override void Fire (){
		if (m_entityTarget.GetComponent<Pawn> () != null) {
			m_entityTarget.GetComponent<Pawn>().Damage(damagePerHit,Pawn.DamageType.MELEE);
		}
	}

}

}
