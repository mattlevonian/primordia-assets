﻿using UnityEngine;
using System.Collections;

namespace Primordia {

	public class Heal : Castable {

		public float healAmount;

		public override GameObject entityTarget {
			set{
				// can't heal self
				if((canTargetSelf || !value.Equals(gameObject)) && value.GetComponent<Pawn>() != null && value.GetComponent<Pawn>().alliance.Equals(gameObject.GetComponent<Pawn>().alliance)){
					m_entityTarget = value;
					GetComponent<Pawn>().StopMoving();
					isAutocasting = true;
				}
			}
		}

		// Use this for initialization
		protected override void Start () {
			base.Start ();
			m_castID = "BaseHeal";
			//canAutocast = true;
			//targetsEntity = true;
		}

		// the method you override in children to do stuff
		protected override void Fire (){
			if (m_entityTarget.GetComponent<Pawn> () != null) {
				m_entityTarget.GetComponent<Pawn>().Heal(healAmount);
			}
		}

	}

}