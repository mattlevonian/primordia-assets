﻿using UnityEngine;
using System.Collections;

namespace Primordia {

	public class Ranged : Castable {
			
		public GameObject projectilePrefab;

		public override GameObject entityTarget {
			set{
				if((canTargetSelf || !value.Equals(gameObject)) && value.GetComponent<Pawn>() != null && !value.GetComponent<Pawn>().alliance.Equals(gameObject.GetComponent<Pawn>().alliance)){
					//print (gameObject.GetComponent<Pawn>().alliance.ToString() + " vs " + value.GetComponent<Pawn>().alliance.ToString());
					m_entityTarget = value;
					GetComponent<Pawn>().StopMoving();
					isAutocasting = true;
				}
			}
		}

		// Use this for initialization
		protected override void Start () {
			base.Start ();
			m_castID = "BaseRanged";
			//canAutocast = true;
			//targetsEntity = true;
		}

		// the method you override in children to do stuff
		protected override void Fire (){
			if (m_entityTarget != null && m_entityTarget.GetComponent<Pawn> () != null) {
				GameObject projectile = (GameObject) Instantiate(projectilePrefab);
				projectile.GetComponent<AestheticProjectile>().start = this.transform;
				projectile.GetComponent<AestheticProjectile>().target = m_entityTarget.transform;
				projectile.GetComponent<AestheticProjectile>().Deploy();
			}
		}

	}

}
