﻿using UnityEngine;
using System.Collections;

namespace Primordia {

public class Constants {

	public static string GAME_MASTER_TAG = "GameMaster";
	public static string PLAYER_CHAR_TAG = "PlayerChar";

	public static string SELECT_KEY_1 = "Select1";
	public static string SELECT_KEY_2 = "Select2";
	public static string SELECT_KEY_3 = "Select3";
	public static string SELECT_KEY_4 = "Select4";

}

}