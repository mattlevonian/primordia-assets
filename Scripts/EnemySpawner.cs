﻿using UnityEngine;
using System.Collections;

namespace Primordia {

	public class EnemySpawner : MonoBehaviour {

		public GameObject enemyPrefab;
		public float spawnPeriod;

		private float lastSpawn;

		public int spawnLimit;
		private int spawnCount;

		void Start(){
			lastSpawn = Time.time;
			spawnCount = 0;
		}

		// Update is called once per frame
		void Update () {
			if (spawnLimit < 0 || spawnCount < spawnLimit) {
				if (Time.time - lastSpawn > spawnPeriod) {
					Spawn ();
					lastSpawn = Time.time;
				}
			}
		}

		public void Spawn(){
			GameObject newenemy = (GameObject)Instantiate (enemyPrefab);
			newenemy.transform.position = transform.position;
			spawnCount += 1;
		}
	}

}
