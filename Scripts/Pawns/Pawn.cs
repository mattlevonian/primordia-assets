﻿using UnityEngine;
using System.Collections;

namespace Primordia {

	// the basic extendable class for a character that moves around in the world

	// castables are added as components of the gameobject, and represent everything from spells to melee attacks

	public abstract class Pawn : MonoBehaviour {
		
		// texture to draw for healthbar (Temp?)
		public Texture2D healthBarTexture;
		public Texture2D baseHealthBarTexture;

		// where the pawn is trying to pathfind to
		protected Vector3 m_pathfinding_target;

		protected bool lowPriorityPathfindingAllowed;

		// the castable that will fire if the player doesn't specify (e.g. melee attacks)
		protected Castable m_default_castable;

		public enum Alliance
		{
			GAIA,
			VEX,
			PLAYER
		}
		public Alliance alliance;

		// hit points
		protected float m_health;
		public float health{
			get{
				return m_health;		
			}
		}

		// max hitpoints
		public float maxHealth;

		protected bool m_dead;
		public bool dead {
			get {
				return m_dead;
			}
		}

		// external health modifier methods

		// bring a pawn back from the dead, starting with a certain amount of health
		// (only works if the pawn is dead)
		public void Revive(float amount){
			if (m_dead) {
				m_dead = false;
				m_health = Mathf.Min (amount, maxHealth);
			}
		}
		// add health beyond max health
		public void BuffHealth(float amount){
			if (!m_dead) {
				m_health = m_health + amount;
			}
		}
		// add health up to max health
		public void Heal(float amount){
			if (!m_dead) {
				m_health = Mathf.Min (m_health + amount, maxHealth);
			}
		}
		// damage types (fire, melee, ranged, magic, etc.)
		// TODO setup damage types so they can be added together as a 'mask' (e.g. FIRE | RANGED for a ranged fire attack)
		public enum DamageType
		{
			GENERIC,
			MELEE,
			RANGED
		}
		// subtract health down to zero (dead)
		public void Damage(float amount, DamageType damageType){
			m_health = Mathf.Max (m_health - amount, 0);

			if (m_health<=0){
				m_dead = true;
				OnDeath();
			}
		}
		public void Damage(float amount){
			Damage (amount, DamageType.GENERIC);
		}

		// units per second walking speed
		public float speed;

		// Use this for initialization
		protected virtual void Start () {
			m_health = maxHealth;
			m_pathfinding_target = transform.position;

			foreach (Castable castable in gameObject.GetComponents<Castable>()) {
				if(castable.isDefault){
					m_default_castable = castable;
					break;
				}
			}
		}

		protected virtual void FixedUpdate () {
			if (!m_dead) {
				//TODO good pathfinding
				Vector3 direction = m_pathfinding_target - transform.position;
				direction.Normalize ();
				rigidbody.velocity = speed * direction;

				if ((m_pathfinding_target - transform.position).magnitude < speed * Time.fixedDeltaTime) {
						//Debug.Log(ToString() + " reached its pathfinding target");
						//m_pathfinding_target = transform.position;
						rigidbody.velocity = Vector3.zero;
				}
			}else if(!rigidbody.isKinematic){
				rigidbody.velocity = Vector3.zero;
			}
		}

		protected void OnGUI(){
			
			// 1. find location of healthbar in world
			Vector3 healthbar_location = transform.position;
			
			// 2. transform to location in screen space
			Vector3 screenspace_location = Camera.main.WorldToScreenPoint (healthbar_location);
			
			// 3. find current health percentage
			float health_fraction = m_health / maxHealth;
			
			float healthBarWidth = 30;
			float healthBarHeight = 4;
			
			// 3. draw healthbar at location
			GUI.DrawTexture(new Rect(screenspace_location.x - healthBarWidth/2, Screen.height - screenspace_location.y - 30f, 				    healthBarWidth, healthBarHeight), baseHealthBarTexture);
			GUI.DrawTexture(new Rect(screenspace_location.x - healthBarWidth/2, Screen.height - screenspace_location.y - 30f, health_fraction * healthBarWidth, healthBarHeight), healthBarTexture);
		}

		// when the pawn runs out of health
		protected virtual void OnDeath (){}

		// when the pawn comes back from the dead
		protected virtual void OnRevival (){}

		protected void StopAutocasting(){
			foreach (Castable castable in GetComponents<Castable>()) 
			{
				castable.isAutocasting = false;		
			}
		}
		public void StopMoving(){
			m_pathfinding_target = transform.position;
			rigidbody.velocity = Vector3.zero;
			lowPriorityPathfindingAllowed = true;
		}

		// External action hooks

		// ---- SET MOVEMENT TARGET ----
		public void MoveToPoint (Vector3 target) {
			if (!m_dead) {
				m_pathfinding_target = target;
				lowPriorityPathfindingAllowed = false;
				StopAutocasting();
			}
		}

		public void MoveToPoint_LowPriority (Vector3 target) {
			if (lowPriorityPathfindingAllowed) {
				// note we aren't called MoveToPoint because that would disable low priority pathfinding
				if (!m_dead) {
					m_pathfinding_target = target;
				}
			}
		}

		// ---- SET CASTING TARGETS ----
		public void SetCastAtPoint (Vector3 target){
			if (!m_dead) {
				foreach (Castable castable in gameObject.GetComponents<Castable>()) {
						castable.worldTarget = target;
				}
				lowPriorityPathfindingAllowed = true;
			}
		}
		public void SetCastAtEntity (GameObject target){
			if (!m_dead) {
				foreach (Castable castable in gameObject.GetComponents<Castable>()) {
						castable.entityTarget = target;
				}
			}
			lowPriorityPathfindingAllowed = true;
		}

		// ---- FIRE CASTABLES ----
		public void FireDefaultCastable(){
			if (!m_dead) {
				if (m_default_castable != null) {
						m_default_castable.Cast ();
				}
				lowPriorityPathfindingAllowed = true;
			}
		}
		public void FireCastable(string castID){
			if(!m_dead){
				foreach (Castable castable in gameObject.GetComponents<Castable>()) {
					if(castable.castID.Equals(castID)){
						castable.Cast();
						return;
					}
				}
				lowPriorityPathfindingAllowed = true;
			}
		}

	}

}