﻿using UnityEngine;
using System.Collections;

namespace Primordia {

public class PlayerCharacter : Pawn {

	// child object with selection circle
	public GameObject selected_marker;

	// which number on the keyboard selects this character
	public int characterOrder;

	private bool m_selected;
	public bool selected{
		get{
			return m_selected;
		}
		set{
			if(value){
				//Debug.Log(this.ToString() + " selected.");
				selected_marker.renderer.enabled = true;
			}else{
				//Debug.Log(this.ToString() + " deselected.");
				selected_marker.renderer.enabled = false;
			}
			m_selected = value;
		}
	}

	// Use this for initialization
	protected override void Start () {
		base.Start ();
		selected = false;
	}

	protected override void OnDeath(){
		base.OnDeath ();
		rigidbody.isKinematic = true;
		StopAutocasting ();
	}

	protected override void OnRevival(){
		base.OnRevival ();
		rigidbody.isKinematic = false;
	}

}

}
