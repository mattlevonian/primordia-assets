﻿using UnityEngine;
using System.Collections;

namespace Primordia {

	public class DevEnemy : Pawn {

		protected GameObject currentTarget;

		protected override void Start () {
			base.Start ();
			AcquireNewTarget ();
		}

		protected void AcquireNewTarget(){
			// find nearest non-dead player character
			float leastDistance = Mathf.Infinity;
			GameObject closestTarget = null;
			foreach (GameObject ch in GameObject.FindGameObjectsWithTag(Constants.PLAYER_CHAR_TAG)) {
				if(ch.GetComponent<PlayerCharacter>() != null && !ch.GetComponent<PlayerCharacter>().dead){
					if(leastDistance > Vector3.Distance(ch.transform.position, transform.position)){
						leastDistance = Vector3.Distance(ch.transform.position, transform.position);
						closestTarget = ch;
					}
				}
			}
			if (closestTarget != null) {
				currentTarget = closestTarget;
				SetCastAtEntity (currentTarget);
				FireDefaultCastable ();
			}
		}

		protected void Update(){
			if (currentTarget!=null && currentTarget.GetComponent<Pawn>() != null && currentTarget.GetComponent<Pawn> ().dead) {
				AcquireNewTarget();
			}
		}

		protected override void OnDeath()
		{
			base.OnDeath ();
			Destroy (gameObject);
		}

	}

}