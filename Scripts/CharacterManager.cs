﻿using UnityEngine;
using System.Collections;

namespace Primordia {

//this should eventually handle GUI for performance increase, right? whatever, maybe this class is useless
public class CharacterManager : MonoBehaviour {

	// size of array (this isn't going to change during a scene, right?)
	public static int MaxPlayerChars = 4;

	// internal array of PC GameObjects
	private GameObject[] m_player_chars;

	// called when the script is first ENABLED
	void Start () {

		// initialize array
		m_player_chars = new GameObject[MaxPlayerChars];

		int index = 0;

		// populate player character list
		foreach (GameObject go in GameObject.FindGameObjectsWithTag(Constants.PLAYER_CHAR_TAG)) 
		{
				m_player_chars[index] = go;
				index += 1;

				if(index>MaxPlayerChars){
					break;
				}
		}
	}
	

}

}