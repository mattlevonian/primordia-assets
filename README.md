# README #

This repository, currently named Primordia, is the primary source version control for the as-yet-unnamed AGP project led by Alejandro Grossman.

## How is the repo structured? ##

This repo is currently structured using the system outlined in the second answer to this question:

http://answers.unity3d.com/questions/18280/how-does-unity-free-source-control-go-wrong.html

*__masterassets__ is our mainline git repository for code, art, etc. This repository is treated just like normal version control -- edit, commit, merge, etc. and push to origin when you want to share with the rest of the team.*

*__scene-master__ is a second git repository for our scene. This repository is treated as unmergeable -- only one person at a time can make changes that they plan to push out the rest of the team. Under the __scene-master__ folder, the Assets folder is a symbolic link to the __masterassets__ repository.*

*For day-to-day development, we clone a copy of the __scene-master__ repository to a __scene-local__ folder. Changes made here are never pushed back to the shared mainline, but can be committed locally for testing.*

*Git is nice and flexible for pulling down multiple copies of repositories to different local folders, so if I want to see the latest stuff from everyone else I can pull it down as a copy without disrupting my local work in progress.*

*When you want to share scene changes with the team, you need to update your __masterassets__ (to get the latest code and art), then delete __scene-master__ and re-clone, to make sure you have the latest scene. Then make your changes, commit everything back to __scene-master__, and push back to the shared repository. We commit everything under the root folder, including Librarycache, etc. After cloning the scene repository, next time you start Unity it will reimport all the assets, which can take a while, depending on your data. If you're worried about having one git repository inside another (the Assets folder points to the __masterassets__ repository), don't -- git is clever enough not to include the nested repository.*

### This is the __masterassets__ repository. You can find the project folder repo [here](https://bitbucket.org/mattlevonian/primordia-build). ###

## How do I get set up? ##

Right now, there are two branches, the master branch and the dev branch. The master branch is the latest, fully-stable release. The dev branch is the current working dev build. All work gets pushed into the dev branch. When the dev build is stable, the master pulls the dev branch changes.

First, go over to the build repo and clone it to a local folder called "scene-local" or something else. Then clone this repo to a different directory called "masterassets" or something else.

Then you need to create a [directory symlink](http://www.tech-recipes.com/rx/13085/windows-7-how-to-create-symlinks-symbolic-links/) called "Assets" in the Unity project folder ("scene-local") that links the "masterassets" directory. 

While working like this, you can't make changes to project settings (they won't get saved unless you push, which you shouldn't). You should only push changes to the Assets folder.

It is recommended that team members download a client like SourceTree, as it makes it easier to work with git. In case you are new to git, here is a basic rundown.

1. Clone the branch you are working off of into a local repository on your computer.
2. Change files locally. 
3. Commit changes to your local repository.
4. Once you have a somewhat stable build, first make sure your local repository is up to date by pulling from the cloud to your local clone.
5. After solving any merge issues, push your local changes to the branch in the cloud.
6. (Optional) If you aren't working off the master branch, you can submit a pull request to the main branch. If your request is approved, the master branch will merge in the changes from your branch. Make sure to pull the latest changes from the master to your working branch first, however.

As the team expands, it is very likely this will be branched off to a working Art branch, Engineering branch, Design Branch, et cetera. Within those teams, it might be beneficial to create a branch for each member (so the merge process is as painless as possible, and so you don't have to keep all your work on the local clone).

## Contribution guidelines ##

Talk to your team lead for specific instructions. Remember to comment your code!

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo) if you intend to add to the project wiki (and you should!)

## Who do I talk to? ##

This repo was set up by Matt Levonian (elevonian@gmail.com)